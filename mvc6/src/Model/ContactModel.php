<?php

namespace App\Model;

//use Core\App;
use Core\App;
use Core\Kernel\AbstractModel;

class ContactModel extends AbstractModel
{
     protected static $table = 'contact';

     protected $id;
     protected $email;


     // insert
     public static function insert($post)
     {
          App::getDatabase()->prepareInsert(
               "INSERT INTO " . self::$table . " (sujet, email, message, created_at) VALUES (?,?,?, NOW())",
               array($post['sujet'], $post['email'], $post['message'],)
          );
     }

     public static function allDESC()
     {
         return App::getDatabase()->query("SELECT * FROM ".self::getTable() ." ORDER BY created_at DESC",get_called_class());
     }





     /**
      * @return mixed
      */
     public function getId()
     {
          return $this->id;
     }


     /**
      * @return mixed
      */
     public function getSujet()
     {
          return $this->sujet;
     }


     /**
      * @return mixed
      */
     public function getEmail()
     {
          return $this->email;
     }


     /**
      * @return mixed
      */
     public function getMessage()
     {
          return $this->message;
     }
}
