<?php

namespace App\Controller;

use App\Model\ContactModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

/**
 *
 */
class ContactController extends AbstractController
{
     public function index()
     {
          $errors = array();

          if (!empty($_POST['submitted'])) {
               $post = $this->cleanXss($_POST);
               $v = new Validation();
               $errors = $this->validate($v, $post);
               if ($v->isValid($errors)) {
                    ContactModel::insert($post);
                    $this->addFlash('success', 'Message envoyé!!');
                    $this->redirect('');
               }
          }

          $form = new Form($errors);
          $this->render('app.contact.index', array(
               'form' => $form,


          ));
     }

     public function listing()
     {

          $listing_contact = ContactModel::allDESC();

          $this->render('app.contact.listing', array(
               'listing_contact' => $listing_contact,
          ));
     }

     private function validate($v, $post)
     {
          $errors = [];
          $errors['sujet'] = $v->textValid($post['sujet'], 'sujet', 2, 50);
          $errors['email'] = $v->emailValid($post['email']);
          $errors['message'] = $v->textValid($post['message'], 'message', 5, 200);
          return $errors;
     }
}
